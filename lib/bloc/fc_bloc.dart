import 'dart:async';

import 'package:fc_penyakit_anak/models/penyakit.dart';
import 'package:fc_penyakit_anak/models/gejala.dart';
import 'package:fc_penyakit_anak/repository/fc_repo.dart';

class ForwardChainingBloc {
  final _repository = ForwardChainingRepository();
  final _gejalaController = StreamController<List<Gejala>>.broadcast();
  final _penyakitController = StreamController<Penyakit>.broadcast();

  get gejalaStream => _gejalaController.stream;
  get penyakitStream => _penyakitController.stream;

  getAllGejala() async {
    _gejalaController.sink.add(await _repository.getAllGejala());
  }

  getPenyakit(int id) async {
    _penyakitController.sink.add(await _repository.getPenyakit(id));
  }

  dispose() {
    _gejalaController.close();
    _penyakitController.close();
  }
}
