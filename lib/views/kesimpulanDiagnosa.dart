import 'package:fc_penyakit_anak/bloc/fc_bloc.dart';
import 'package:fc_penyakit_anak/constants/strings.dart';
import 'package:fc_penyakit_anak/main.dart';
import 'package:fc_penyakit_anak/models/penyakit.dart';
import 'package:fc_penyakit_anak/models/gejala.dart';
import 'package:flutter/material.dart';

class KesimpulanDiagnosa extends StatefulWidget {
  final int resiko;
  final List gejala;
  KesimpulanDiagnosa({this.resiko, this.gejala});

  @override
  _KesimpulanDiagnosaState createState() => _KesimpulanDiagnosaState();
}

class _KesimpulanDiagnosaState extends State<KesimpulanDiagnosa> {
  final ForwardChainingBloc _bloc = ForwardChainingBloc();
  var saran = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Center(
            child: Container(
              margin: EdgeInsets.fromLTRB(50, 50, 50, 20),
              child: Column(
                children: <Widget>[
                  Image.asset(
                    "images/doctor.png",
                    width: 150,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Kesimpulan Diagnosa",
                    style: TextStyle(
                        color: Color(0xff1B2D49),
                        fontFamily: "WorkSans",
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Kesimpulan didapatkan dari setiap jawaban",
                    style: TextStyle(
                        color: Color(0xff8F8F8F),
                        fontFamily: "Poppins",
                        fontSize: 12),
                  ),
                  Text(
                    "yang sudah anda isi sebelumnya.",
                    style: TextStyle(
                        color: Color(0xff8F8F8F),
                        fontFamily: "Poppins",
                        fontSize: 12),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.asset(
                        "images/box.png",
                        width: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              "Respon anda",
                              style: TextStyle(
                                  color: Color(0xff1B2D49),
                                  fontFamily: "Poppins",
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 10, top: 10),
                              width: 250,
                              child: _responWidget()),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.asset(
                        "images/box.png",
                        width: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              "Resiko",
                              style: TextStyle(
                                  color: Color(0xff1B2D49),
                                  fontFamily: "Poppins",
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 10, top: 10),
                              width: 250,
                              child: _resikoWidget()),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.asset(
                        "images/box.png",
                        width: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              "Saran",
                              style: TextStyle(
                                  color: Color(0xff1B2D49),
                                  fontFamily: "Poppins",
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10, top: 10),
                            width: 250,
                            child: _saranWidget()
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 30),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => MyApp()));
                    },
                    child: Container(
                      height: 40,
                      width: 200,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('images/button.png'))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Kembali ke Beranda",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.w500,
                                fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _resikoWidget() {
    return StreamBuilder(
      stream: _bloc.penyakitStream,
      builder: (BuildContext context, AsyncSnapshot<Penyakit> snapshot) {
        if (snapshot.hasData) {
          Penyakit kesimpulan = snapshot.data;
          return Text(
            "Anak terkena penyakit "+kesimpulan.nama_penyakit.toLowerCase(),
            style: TextStyle(
                color: Color(0xff8F8F8F),
                fontFamily: "Poppins",
                fontSize: 14,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.justify,
          );
        } else {
          _bloc.getPenyakit(widget.resiko);
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
              ],
            ),
          );
        }
      },
    );
  }

  Widget _responWidget() {
    return StreamBuilder(
      stream: _bloc.gejalaStream,
      builder: (BuildContext context, AsyncSnapshot<List<Gejala>> snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: snapshot.data.length,
            itemBuilder: (context, itemPosition) {
              Gejala gejala = snapshot.data[itemPosition];
              return widget.gejala[itemPosition] == 1
                  ? Text(
                      "- " + gejala.nama_gejala,
                      style: TextStyle(
                          color: Color(0xff8F8F8F),
                          fontFamily: "Poppins",
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    )
                  : Container();
            },
          );
        } else {
          _bloc.getAllGejala();
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
              ],
            ),
          );
        }
      },
    );
  }

  Widget _saranWidget() {
    if (widget.gejala[0] == 1 && widget.gejala[1] == 1 && widget.gejala[2] == 1 && widget.gejala[3] == 1 && widget.gejala[4] == 1 && widget.gejala[5] == 1 && widget.gejala[6] == 1) {
      // IF G01 is true AND G02 is true AND G03 is true AND G04 is true AND G05 is true AND G06 is true AND G07 is true THEN P01
      saran.add(solusiPenyakit1);
    } else if (widget.gejala[1] == 1 && widget.gejala[9] == 1 && widget.gejala[11] == 1 && widget.gejala[16] == 1 && widget.gejala[17] == 1 && widget.gejala[18] == 1) {
      // IF G02 is true AND G10 is true AND G12 is true AND G17 is true AND G18 is true AND G19 is true THEN P04
      saran.add(solusiPenyakit4);
    } else if (widget.gejala[2] == 1 && widget.gejala[5] == 1 && widget.gejala[9] == 1 && widget.gejala[11] == 1 && widget.gejala[13] == 1 && widget.gejala[14] == 1 && widget.gejala[15] == 1) {
      // IF G03 is true AND G06 is true AND G10 is true AND G12 is true AND G14 is true AND G15 is true AND G16 is true THEN P03
      saran.add(solusiPenyakit3);
    } else if (widget.gejala[5] == 1 && widget.gejala[6] == 1 && widget.gejala[7] == 1 && widget.gejala[8] == 1 && widget.gejala[9] == 1 && widget.gejala[10] == 1 && widget.gejala[11] == 1 && widget.gejala[12] == 1) {
      // IF G06 is true AND G07 is true AND G08 is true AND G09 is true AND G10 is true AND G11 is true AND G12 is true AND G13 is true THEN P02
      saran.add(solusiPenyakit2);
    } else if (widget.gejala[19] == 1 && widget.gejala[20] == 1 && widget.gejala[21] == 1 && widget.gejala[22] == 1 && widget.gejala[23] == 1 && widget.gejala[24] == 1) {
      // IF G20 is true AND G21 is true AND G22 is true AND G23 is true AND G24 is true AND G25 is true THEN P05
      saran.add(solusiPenyakit5);
    } else {
      saran.add(solusiTidakAda); // DEFAULT SOLUSI
    }

    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: saran.length,
      itemBuilder: (context, itemPosition) {
        return Text(
          saran[itemPosition],
          style: TextStyle(
              color: Color(0xff8F8F8F),
              fontFamily: "Poppins",
              fontSize: 14,
              fontWeight: FontWeight.w500),
        );
      },
    );
  }
}
