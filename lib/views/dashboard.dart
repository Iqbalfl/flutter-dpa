import 'package:fc_penyakit_anak/bloc/api_bloc.dart';
import 'package:fc_penyakit_anak/models/indocovid_response.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  void initState() {
    apiBloc.fetchIndoCovid();
    apiBloc.fetchWorldCovid();
    apiBloc.fetchWorldCovidDeath();
    apiBloc.fetchWorldCovidRecover();
    super.initState();
  }

  String dropdownValue = 'Indonesia';
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
                alignment: Alignment.topRight,
                margin: EdgeInsets.fromLTRB(20, 20, 30, 20),
                child: Text(
                  "DIAGNOSA PENYAKIT ANAK",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'WorkSans',
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                )),
            Container(
                height: 190.0,
                width: 350.0,
                child: Carousel(
                  images: [
                    AssetImage("images/banner-1.png"),
                    AssetImage("images/banner-2.png")
                  ],
                  dotSize: 4.0,
                  dotSpacing: 15.0,
                  dotColor: Colors.grey,
                  indicatorBgPadding: 15.0,
                  dotBgColor: Colors.orange.withOpacity(0),
                  borderRadius: true,
                )),
            Container(
              alignment: Alignment.bottomLeft,
              margin: EdgeInsets.fromLTRB(30, 20, 20, 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Klinik Pratama Mitramedik Arcamanik",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'WorkSans',
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  Text(
                    "Melayani Sepenuh Hati",
                    style: TextStyle(
                        color: Colors.grey,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.normal,
                        fontSize: 12),
                  )
                ],
              ),
            ),           
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('images/blue.png'))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      StreamBuilder(
                        stream: apiBloc.stream,
                        builder: (context,
                            AsyncSnapshot<List<IndoCovidResponse>> snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                              snapshot.data.first.positif,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22),
                            );
                          } else if (snapshot.hasError) {
                            return Text(
                              snapshot.error,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22),
                            );
                          }
                          return Text(
                            "1",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          );
                        },
                      ),
                      Text(
                        "Cepat",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                            fontSize: 10),
                      ),
                      Text(
                        "",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                            fontSize: 10),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image:
                          DecorationImage(image: AssetImage('images/red.png'))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      StreamBuilder(
                        stream: apiBloc.stream,
                        builder: (context,
                            AsyncSnapshot<List<IndoCovidResponse>> snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                              snapshot.data.first.meninggal,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22),
                            );
                          } else if (snapshot.hasError) {
                            return Text(
                              snapshot.error,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22),
                            );
                          }
                          return Text(
                            "2",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          );
                        },
                      ),
                      Text(
                        "Aman",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                            fontSize: 10),
                      ),
                      Text(
                        "",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                            fontSize: 10),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('images/green.png'))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      StreamBuilder(
                        stream: apiBloc.stream,
                        builder: (context,
                            AsyncSnapshot<List<IndoCovidResponse>> snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                              snapshot.data.first.sembuh,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22),
                            );
                          } else if (snapshot.hasError) {
                            return Text(
                              snapshot.error,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22),
                            );
                          }
                          return Text(
                            "3",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          );
                        },
                      ),
                      Text(
                        "Terpercaya",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                            fontSize: 10),
                      ),
                      Text(
                        "",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                            fontSize: 10),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Container(
              alignment: Alignment.bottomLeft,
              margin: EdgeInsets.fromLTRB(30, 20, 20, 20),
              child: Text(
                "Tentang Kami",
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 30),
              height: 190.0,
              width: 350.0,
              child: Column(
                  children: <Widget>[
                    Image.asset('images/banner-3.png'),
                  ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
