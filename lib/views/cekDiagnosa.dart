import 'package:fc_penyakit_anak/views/kesimpulanDiagnosa.dart';
import 'package:flutter/material.dart';

class CekDiagnosa extends StatefulWidget {
  final List jawaban;
  CekDiagnosa(this.jawaban);

  @override
  _CekDiagnosaState createState() => _CekDiagnosaState();
}

class _CekDiagnosaState extends State<CekDiagnosa> {
  var resiko;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.fromLTRB(50, 80, 50, 20),
        child: Column(
          children: <Widget>[
            Image.asset("images/banner-4.png"),
            SizedBox(
              height: 130,
            ),
            Text(
              "Semua pertanyaan sudah terjawab.",
              style: TextStyle(
                  color: Color(0xff1B2D49),
                  fontFamily: "WorkSans",
                  fontSize: 12,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              "Silahkan cek diagnosa anda.",
              style: TextStyle(
                  color: Color(0xff1B2D49),
                  fontFamily: "WorkSans",
                  fontSize: 12,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {
                _forwardChaining(widget.jawaban);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => KesimpulanDiagnosa(resiko: resiko, gejala: widget.jawaban,)));
              },
              child: Container(
                height: 40,
                width: 200,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/button.png'))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Cek Hasil Diagnosa",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w500,
                          fontSize: 12),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _forwardChaining(List jawaban) {
    for (var i = 0; i < jawaban.length; i++) {
      jawaban[i] == null ? jawaban[i] = 0 : jawaban[i] = jawaban[i];
    }

    if (jawaban[0] == 1 && jawaban[1] == 1 && jawaban[2] == 1 && jawaban[3] == 1 && jawaban[4] == 1 && jawaban[5] == 1 && jawaban[6] == 1) {
      // IF G01 is true AND G02 is true AND G03 is true AND G04 is true AND G05 is true AND G06 is true AND G07 is true THEN P01
      resiko = 1;
    } else if (jawaban[1] == 1 && jawaban[9] == 1 && jawaban[11] == 1 && jawaban[16] == 1 && jawaban[17] == 1 && jawaban[18] == 1) {
      // IF G02 is true AND G10 is true AND G12 is true AND G17 is true AND G18 is true AND G19 is true THEN P04
      resiko = 4;
    } else if (jawaban[2] == 1 && jawaban[5] == 1 && jawaban[9] == 1 && jawaban[11] == 1 && jawaban[13] == 1 && jawaban[14] == 1 && jawaban[15] == 1) {
      // IF G03 is true AND G06 is true AND G10 is true AND G12 is true AND G14 is true AND G15 is true AND G16 is true THEN P03
      resiko = 3;
    } else if (jawaban[5] == 1 && jawaban[6] == 1 && jawaban[7] == 1 && jawaban[8] == 1 && jawaban[9] == 1 && jawaban[10] == 1 && jawaban[11] == 1 && jawaban[12] == 1) {
      // IF G06 is true AND G07 is true AND G08 is true AND G09 is true AND G10 is true AND G11 is true AND G12 is true AND G13 is true THEN P02
      resiko = 2;
    } else if (jawaban[19] == 1 && jawaban[20] == 1 && jawaban[21] == 1 && jawaban[22] == 1 && jawaban[23] == 1 && jawaban[24] == 1) {
      // IF G20 is true AND G21 is true AND G22 is true AND G23 is true AND G24 is true AND G25 is true THEN P05
      resiko = 5;
    } else {
      resiko = 1; // DEFAULT PENYAKIT
    }

  }
}
