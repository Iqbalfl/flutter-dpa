import 'package:fc_penyakit_anak/dao/db_dao.dart';

class ForwardChainingRepository {
  final dbDao = DBDao();

  Future getPenyakit(int id) => dbDao.getPenyakit(id);
  Future getAllGejala() => dbDao.getAllGejala();
}
