import 'package:fc_penyakit_anak/constants/strings.dart';
import 'package:fc_penyakit_anak/database/db.dart';
import 'package:fc_penyakit_anak/models/penyakit.dart';
import 'package:fc_penyakit_anak/models/gejala.dart';

class DBDao {
  final dbProvider = DatabaseProvider.dbProvider;

  Future<List<Gejala>> getAllGejala() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    result = await db.rawQuery("SELECT * FROM $tableGejala");

    List<Gejala> data = result.isNotEmpty
        ? result.map((item) => Gejala.fromMap(item)).toList()
        : [];
    return data;
  }

  Future<Penyakit> getPenyakit(int id) async {
    final db = await dbProvider.database;
    List<Map> maps = await db.query(tablePenyakit,
        columns: [columnId, columnNamaPenyakit],
        where: '$columnId = ?',
        whereArgs: [id]);

    return maps.isNotEmpty ? Penyakit.fromMap(maps.first) : null;
  }
}
