final String columnId = '_id';

final String tableGejala = 'gejala';
final String columnNamaGejala = 'nama_gejala';

final String tablePenyakit = 'penyakit';
final String columnNamaPenyakit = 'nama_penyakit';

final String tableAturan = 'aturan';
final String columnIdPenyakit = 'id_penyakit';
final String columnIdGejala = 'id_gejala';

final String gejala1 = "Bab cair lebih dari 3x sehari";
final String gejala2 = "Lesu";
final String gejala3 = "Nafsu makan berkurang";
final String gejala4 = "Keram pada perut";
final String gejala5 = "Perut kembung";
final String gejala6 = "Demam";
final String gejala7 = "Muntah";
final String gejala8 = "Kejang 1-2x sehari";
final String gejala9 = "Bab cair";
final String gejala10 = "Sesak nafas";
final String gejala11 = "Terlihat sangat mengantuk";
final String gejala12 = "Batuk";
final String gejala13 = "Pilek";
final String gejala14 = "Mengigil";
final String gejala15 = "Dada terasa sakit";
final String gejala16 = "Sakit kepala";
final String gejala17 = "Nafsu berbuni (mengi)";
final String gejala18 = "Faktor keturunan";
final String gejala19 = "Susah tidur";
final String gejala20 = "Anak tampak kurus";
final String gejala21 = "Pucat";
final String gejala22 = "Gatal sekitar anus";
final String gejala23 = "Gelisah atau tidak nyaman saat tidur";
final String gejala24 = "Iritasi kulit sekitar anus";
final String gejala25 = "Sering sakit perut";

final String penyakit1 = "Diare";
final String penyakit2 = "Kejang Demam";
final String penyakit3 = "Bronchopneumonia";
final String penyakit4 = "Asma";
final String penyakit5 = "Cacingan";

// TODOS: RUBAH SOLUSI SESUAI PENYAKIT
final String solusiPenyakit1 = '''Anda tidak memiliki faktor risiko penularan virus Corona, seperti bepergian ke negara dengan wabah virus Corona, ataupun kontak dengan orang memiliki gejala flu, diduga terinfeksi Corona, atau bahkan sudah positif terinfeksi virus Corona.''';
final String solusiPenyakit2 = '''Keluhan yang Anda alami kemungkinan disebabkan oleh adanya infeksi paru-paru atau flu biasa.''';
final String solusiPenyakit3 = '''Corona di nomor 119 ext. 9 dan ikuti instruksi yang diberikan. \n- Jangan tinggalkan rumah kecuali untuk berobat, dan pastikan Anda berada di ruangan terpisah dari anggota keluarga, kerabat, atau orang lain, untuk mencegah penularan. \n- Gunakan masker selama masih sakit. \n- Ikuti etika batuk/bersin yang benar. Caranya, tutup mulut dan hidung dengan tisu atau lengan bagian dalam, lalu buang tisu yang telah digunakan ke tempat sampah.''';
final String solusiPenyakit4 = '''Virus Corona merupakan virus yang menyerang saluran pernapasan. Gejala yang timbul bila terinfeksi virus ini bisa ringan sampai berat. Gejala yang ringan umumnya seperti gejala flu, yakni demam, batuk, pilek, dan sakit tenggorokan. Sebagian kecil kasus bisa berkembang menjadi berat dan menyebabkan infeksi paru-paru atau pneumonia. Meski begitu, infeksi virus ini juga bisa tidak menimbulkan gejala apa-apa''';
final String solusiPenyakit5 = '''Virus Corona merupakan virus yang menyerang saluran pernapasan. Gejala yang timbul bila terinfeksi virus ini bisa ringan sampai berat. Gejala yang ringan umumnya seperti gejala flu, yakni demam, batuk, pilek, dan sakit tenggorokan. Sebagian kecil kasus bisa berkembang menjadi berat dan menyebabkan infeksi paru-paru atau pneumonia. Meski begitu, infeksi virus ini juga bisa tidak menimbulkan gejala apa-apa''';

final String solusiTidakAda = '''Mohon maaf sepertinya penyakit dari gejala tersebut belum bisa kami diagnosa''';