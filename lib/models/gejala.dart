import 'package:fc_penyakit_anak/constants/strings.dart';

class Gejala {
  int id;
  String nama_gejala;

  Gejala({this.id, this.nama_gejala});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnNamaGejala: nama_gejala,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  Gejala.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    nama_gejala = map[columnNamaGejala];
  }
}
