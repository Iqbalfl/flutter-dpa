import 'package:fc_penyakit_anak/constants/strings.dart';

class Penyakit {
  int id;
  String nama_penyakit;

  Penyakit({this.id, this.nama_penyakit});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnNamaPenyakit: nama_penyakit,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  Penyakit.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    nama_penyakit = map[columnNamaPenyakit];
  }
}
