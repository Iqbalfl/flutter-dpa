import 'dart:io';

import 'package:fc_penyakit_anak/constants/strings.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static final DatabaseProvider dbProvider = DatabaseProvider();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

  // Future onConfigure(Database db) async {
  //   await db.execute('PRAGMA foreign_keys = ON');
  // }

  createDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    //"ReactiveTodo.db is our database instance name
    String path = join(documentsDirectory.path, "dpa.db");

    var database = await openDatabase(
      path,
      version: 1,
      onCreate: initDB,
      // onUpgrade: onUpgrade,
      // onConfigure: onConfigure
    );
    return database;
  }

  //This is optional, and only used for changing DB schema migrations
  // void onUpgrade(Database database, int oldVersion, int newVersion) {
  //   if (newVersion > oldVersion) {}
  // }

  void initDB(Database database, int version) async {
    //CREATE
    await database.execute('''
        create table $tableGejala ( 
          $columnId integer primary key autoincrement, 
          $columnNamaGejala text not null)
        ''');

    await database.execute('''
        create table $tablePenyakit ( 
          $columnId integer primary key autoincrement, 
          $columnNamaPenyakit text not null)
        ''');

    await database.execute('''
        create table $tableAturan (
          $columnId integer primary key autoincrement,
          $columnIdPenyakit integer not null,
          $columnIdGejala integer not null)
        ''');

    // INSERT
    // Tabel Gejala
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala1"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala2"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala3"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala4"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala5"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala6"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala7"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala8"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala9"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala10"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala11"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala12"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala13"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala14"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala15"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala16"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala17"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala18"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala19"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala20"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala21"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala22"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala23"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala24"]);
    await database.rawInsert(
        'INSERT INTO $tableGejala ($columnNamaGejala) VALUES(?)',
        ["$gejala25"]);

    // Tabel Penyakit
    await database.rawInsert(
        'INSERT INTO $tablePenyakit ($columnNamaPenyakit) VALUES(?)',
        ["$penyakit1"]);
    await database.rawInsert(
        'INSERT INTO $tablePenyakit ($columnNamaPenyakit) VALUES(?)',
        ["$penyakit2"]);
    await database.rawInsert(
        'INSERT INTO $tablePenyakit ($columnNamaPenyakit) VALUES(?)',
        ["$penyakit3"]);
    await database.rawInsert(
        'INSERT INTO $tablePenyakit ($columnNamaPenyakit) VALUES(?)',
        ["$penyakit4"]);
    await database.rawInsert(
        'INSERT INTO $tablePenyakit ($columnNamaPenyakit) VALUES(?)',
        ["$penyakit5"]);
  }
}
